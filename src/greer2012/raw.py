"""
Raw formalisms from paper
"""
import math

ca = 389  # [µmol.mol-1] from below eq12


def eq10(ppfd, a_max, alpha_app, rd):
    """Net photosynthesis.

    Args:
        ppfd (float): [µmol photon.m-2.s-1] absorbed photon irradiance
        a_max (float): [µmol CO2.m-2.s-1] Max rate of photosynthesis
        alpha_app (float): [mol CO2.mol photon-1] apparent CO2 limited photon yield
        rd (float): [µmol CO2.m-2.s-1] dark respiration

    Returns:
        an (float): [µmol CO2.m-2.s-1]
    """
    return a_max * math.tanh(ppfd * alpha_app / a_max) - rd


def eq11(a_max, alpha_app):
    """Saturating radiation

    Args:
        a_max (float): [µmol CO2.m-2.s-1] Max rate of photosynthesis
        alpha_app (float): [mol CO2.mol photon-1] apparent CO2 limited photon yield

    Returns:
        ppfd_sat (float): [µmol photon.m-2.s-1]
    """
    return math.atanh(0.99) * a_max / alpha_app
