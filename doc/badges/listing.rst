Listing
========

.. {# pkglts, glabpkg_dev

.. image:: badge_doc.svg
    :alt: documentation

.. #}
.. {# pkglts, glabpkg, after glabpkg_dev

.. image:: badge_pkging_pip.svg
    :alt: PyPI version

.. image:: badge_pkging_conda.svg
    :alt: conda version

.. #}
