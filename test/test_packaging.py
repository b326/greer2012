# {# pkglts, glabpkg_dev
import greer2012


def test_package_exists():
    assert greer2012.__version__

# #}
# {# pkglts, glabdata, after glabpkg_dev

def test_paths_are_valid():
    assert greer2012.pth_clean.exists()
    try:
        assert greer2012.pth_raw.exists()
    except AttributeError:
        pass  # package not installed in editable mode

# #}
