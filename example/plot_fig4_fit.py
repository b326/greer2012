"""
Figure 4 (fitted)
=================

Plot fig4 using curve and fit from article.
"""
from math import atanh

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from greer2012 import pth_clean, raw

# read data
df = pd.read_csv(pth_clean / "fig4.csv", sep=";", comment="#")
pfit = pd.read_csv(pth_clean / "fig5_params.csv", sep=";", comment="#", index_col=['name'])


def poly(x, params):
    return sum(coeff * pow(x, n) for n, coeff in enumerate(params))


# estimate curves
ppfds = np.linspace(0, 2000, 100)

estim = {}
for t_leaf in df['temp'].unique():
    an_max = poly(t_leaf, pfit.loc['an_max'])
    ppfd_sat = poly(t_leaf, pfit.loc['ppfd_sat'])
    alpha_app = an_max / ppfd_sat * atanh(0.99)
    rd = 0.82

    estim[t_leaf] = [raw.eq10(ppfd, an_max, alpha_app, rd) for ppfd in ppfds]

# plot data
fig, axes = plt.subplots(1, 1, figsize=(10, 6), squeeze=False)

ax = axes[0, 0]
for temp, sdf in df.groupby(by='temp'):
    crv, = ax.plot(sdf['ppfd'], sdf['assim'], 'o-', label=f"{temp:d}°C")
    ax.plot(ppfds, estim[temp], '--', color=crv.get_color())

ax.legend(loc='lower right')
ax.set_ylim(-5, 20)
ax.set_xlabel("PPFD [µmol photon.m-2.s-1]")
ax.set_ylabel("Assimilation [µmol CO2.m-2.s-1]")

fig.tight_layout()
plt.show()
