"""
Figure 11
=========

Plot vc_max and j_max evolution with temperature.
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from medlyn2002 import raw
from medlyn2002.external import kelvin

from greer2012 import pth_clean

# read data
meas = pd.read_csv(pth_clean / "fig11.csv", sep=";", comment="#", index_col=['temp'])
pfit = pd.read_csv(pth_clean / "fig11_params.csv", sep=";", comment="#", index_col=['name', 'sub'])['value']
tab = pd.read_csv(pth_clean / "estims.csv", sep=";", comment="#", index_col=['name'])
pval = tab['value'].to_dict()

for name, val in tuple(pval.items()):
    if 'ha' in name or 'hd' in name:
        pval[name] = val * 1e3  # [kJ.mol-1] to [J.mol-1]

pval['j_max_ds'] = raw.eq19_inv(pval['j_max_ha'], pval['j_max_hd'], kelvin(pval['j_max_t_opt']))
pval['vc_max_ds'] = raw.eq19_inv(pval['vc_max_ha'], pval['vc_max_hd'], kelvin(pval['vc_max_t_opt']))

# compute curves
records = []
for t_leaf in np.linspace(20, 45, 100):
    tk = kelvin(t_leaf)

    j_max = raw.eq17(tk, pval['j_max_25'], pval['j_max_ha'], pval['j_max_hd'], pval['j_max_ds'])
    j_max_alt = raw.eq18(tk,
                         kelvin(pfit.at[('j_max', 't_opt')]),
                         pfit.at[('j_max', 'k_opt')],
                         pfit.at[('j_max', 'ha')],
                         pfit.at[('j_max', 'hd')])
    vc_max = raw.eq17(tk, pval['vc_max_25'], pval['vc_max_ha'], pval['vc_max_hd'], pval['vc_max_ds'])
    vc_max_alt = raw.eq18(tk,
                          kelvin(pfit.at[('vc_max', 't_opt')]),
                          pfit.at[('vc_max', 'k_opt')],
                          pfit.at[('vc_max', 'ha')],
                          pfit.at[('vc_max', 'hd')])
    records.append(dict(
        t_leaf=t_leaf,
        j_max=j_max,
        j_max_alt=j_max_alt,
        vc_max=vc_max,
        vc_max_alt=vc_max_alt,
    ))

df = pd.DataFrame(records).set_index('t_leaf')

# plot data
fig, axes = plt.subplots(2, 1, sharex='all', figsize=(7, 6), squeeze=False)

ax = axes[0, 0]
ax.plot(meas.index, meas['vc_max'], 'o', label="meas")
vc_max_fit = meas['vc_max_fit'].dropna()
ax.plot(vc_max_fit.index, vc_max_fit, label="paper")
ax.plot(df.index, df['vc_max'], '--', label="params from paper")
ax.plot(df.index, df['vc_max_alt'], '--', label="using opt")

ax.legend(loc='upper left')
ax.set_ylim(0, 135)
ax.set_ylabel("vc_max [µmol CO2.m-2.s-1]")

ax = axes[1, 0]
ax.plot(meas.index, meas['j_max'], 'o', label="meas")
j_max_fit = meas['j_max_fit'].dropna()
ax.plot(j_max_fit.index, j_max_fit, label="paper")
ax.plot(df.index, df['j_max'], '--', label="params from paper")
ax.plot(df.index, df['j_max_alt'], '--', label="using opt")

ax.legend(loc='upper left')
ax.set_ylim(0, 200)
ax.set_ylabel("j_max [µmol CO2.m-2.s-1]")

ax.set_xlabel("t_leaf [°C]")

fig.tight_layout()
plt.show()
