"""
Vcmax and Jmax dependency on temperature
========================================

Evolution of vc_max and j_max with temperature. Similar to fig11 but on same axes.
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from medlyn2002 import raw
from medlyn2002.external import kelvin

from greer2012 import pth_clean

# read data
pfit = pd.read_csv(pth_clean / "fig11_params.csv", sep=";", comment="#", index_col=['name', 'sub'])['value']

# compute curves from formalisms
records = []
for t_leaf in np.linspace(5, 40, 100):
    tk = kelvin(t_leaf)

    j_max = raw.eq18(tk,
                     kelvin(pfit.at[('j_max', 't_opt')]),
                     pfit.at[('j_max', 'k_opt')],
                     pfit.at[('j_max', 'ha')],
                     pfit.at[('j_max', 'hd')])

    vc_max = raw.eq18(tk,
                      kelvin(pfit.at[('vc_max', 't_opt')]),
                      pfit.at[('vc_max', 'k_opt')],
                      pfit.at[('vc_max', 'ha')],
                      pfit.at[('vc_max', 'hd')])
    records.append(dict(
        t_leaf=t_leaf,
        j_max=j_max,
        vc_max=vc_max,
    ))

df = pd.DataFrame(records).set_index('t_leaf')

# plot data
fig, axes = plt.subplots(1, 1, figsize=(8, 6), squeeze=False)
ax = axes[0, 0]
ax.plot(df.index, df['vc_max'], label="vc_max")
ax.plot(df.index, df['j_max'], label="j_max")

ax.legend(loc='upper left')
ax.set_ylabel("[µmol.m-2.s-1]")
ax.set_xlabel("t_leaf [°C]")

fig.tight_layout()
plt.show()
