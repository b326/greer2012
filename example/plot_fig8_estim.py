"""
Figure 8 (estimated)
====================

Use parameters and formalisms to reproduce fig8.
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from medlyn2002 import raw
from medlyn2002.external import kelvin

from greer2012 import pth_clean

# read data
meas = pd.read_csv(pth_clean / "fig8.csv", sep=";", comment="#", index_col=['cc'])
pfit = pd.read_csv(pth_clean / "fig11_params.csv", sep=";", comment="#", index_col=['name', 'sub'])['value']

# simulate curve
rd = 0.82  # [µmol CO2.m-2.s-1] from Harley 1992 table 1

# from Medlyn
alpha = 0.3  # [mol electron.mol photon-1] from p1170 end of Model section
theta = 0.9  # [-] from p1170 end of Model section
oi = 210  # [mmol.mol-1] from below eq11

ppfd = 900  # µmol photon.m-2.s-1] saturating?

dfs = []
for t_leaf in (20, 35):
    tk = kelvin(t_leaf)

    kc = raw.eq5(tk)
    ko = raw.eq6(tk)
    gamma_star = raw.eq12(tk)
    print(f"t_leaf {t_leaf:d}, gamma* {gamma_star:.1f}, km {kc * (1 + oi / ko):.1f}")

    j_max = raw.eq18(tk,
                     kelvin(pfit.at[('j_max', 't_opt')]),
                     pfit.at[('j_max', 'k_opt')],
                     pfit.at[('j_max', 'ha')],
                     pfit.at[('j_max', 'hd')])
    vc_max = raw.eq18(tk,
                      kelvin(pfit.at[('vc_max', 't_opt')]),
                      pfit.at[('vc_max', 'k_opt')],
                      pfit.at[('vc_max', 'ha')],
                      pfit.at[('vc_max', 'hd')])

    print("t_leaf", t_leaf, vc_max)
    records = []
    for ci in np.linspace(0, 1400, 100):
        j = raw.eq4(ppfd, j_max, alpha, theta)
        ac = raw.eq2(ci, vc_max, gamma_star, kc, ko, oi)
        aj = raw.eq3(j, ci, gamma_star)
        an = raw.eq1(ac, aj, rd)

        records.append(dict(
            t_leaf=t_leaf,
            ci=ci,
            an=an,
            ac=ac,
            aj=aj,
        ))

    df = pd.DataFrame(records)
    dfs.append([t_leaf, df])

# plot data
fig, axes = plt.subplots(2, 1, figsize=(8, 8), squeeze=False)

for i, (t_leaf, df) in enumerate(dfs):
    ax = axes[i, 0]
    ax.plot(meas.index, meas[f'an_{t_leaf:.0f}'], 'o')
    ac_fit = meas[f'ac_{t_leaf:.0f}'].dropna()
    crv, = ax.plot(ac_fit.index, ac_fit, '-', label="ac")
    ax.plot(df['ci'], df['ac']*2 - rd, '--', color=crv.get_color(), label="params from paper")
    aj_fit = meas[f'aj_{t_leaf:.0f}'].dropna()
    crv, = ax.plot(aj_fit.index, aj_fit, '-', label="aj")
    ax.plot(df['ci'], df['aj'] - rd, '--', color=crv.get_color(), label="params from paper")

    ax.legend(loc='lower right', title=f"t_leaf: {t_leaf:.0f}")
    ax.set_ylim(-5, 35)
    ax.set_xlabel("Ci [µmol.mol-1]")
    ax.set_ylabel("Assimilation [µmol CO2.m-2.s-1]")

fig.tight_layout()
plt.show()
