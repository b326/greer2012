"""
Figure 5
========

Plot response to temperature of assimilation max curves to compare to fig5.
"""
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from medlyn2002.raw import eq18

from greer2012 import pth_clean
from greer2012.external import kelvin, celsius

# read data
df = pd.read_csv(pth_clean / "fig5.csv", sep=";", comment="#")
pfit = pd.read_csv(pth_clean / "fig5_params.csv", sep=";", comment="#", index_col=['name'])


def crv(x, params):
    return sum(coeff * pow(x, n) for n, coeff in enumerate(params))


# plot data
fig, axes = plt.subplots(3, 1, sharex='all', figsize=(8, 7), squeeze=False)

ax = axes[0, 0]
ax.plot(df['temp'], df['an_max'], 'o-')
ts = np.linspace(20, 40, 100)
opt = pfit.loc['an_max']
ax.plot(ts, [eq18(kelvin(t), **opt) for t in ts], '-')
# ax.text(25, 10, f"amax={opt_an_max[0]:.2f} {opt_an_max[1]:+.2f}*temp {opt_an_max[2]:+.2f}*temp^2")
ax.text(25, 10, f"t_opt={celsius(opt['t_opt']):.1f}, k_opt={opt['k_opt']:.1f}")
ax.set_ylim(0, 22.5)
ax.set_ylabel("an_max [µmol CO2.m-2.s-1]")

ax = axes[1, 0]
ax.plot(df['temp'], df['py'], 'o-')
ax.set_ylim(0, 0.07)
ax.set_ylabel("py [mol.mol-1]")

ax = axes[2, 0]
ax.plot(df['temp'], df['ppfd_sat'], 'o-')
ts = np.linspace(20, 40, 100)
opt = pfit.loc['ppfd_sat']
ax.plot(ts, [eq18(kelvin(t), **opt) for t in ts], '-')
# ax.text(25, 500, f"ppfd_sat={opt_ppfd_sat[0]:.2f} {opt_ppfd_sat[1]:+.2f}*temp {opt_ppfd_sat[2]:+.2f}*temp^2")
ax.text(25, 500, f"t_opt={celsius(opt['t_opt']):.1f}, k_opt={opt['k_opt']:.0f}")
ax.set_ylim(0, 1500)
ax.set_xlabel("Leaf temperature [°C]")
ax.set_ylabel("ppfd_sat [µmol photon.m-2.s-1]")

fig.tight_layout()
plt.show()
