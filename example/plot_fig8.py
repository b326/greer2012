"""
Figure 8
========

Plot Assimilation light response curves to compare to fig8.
"""
import matplotlib.pyplot as plt
import pandas as pd

from greer2012 import pth_clean

# read data
meas = pd.read_csv(pth_clean / "fig8.csv", sep=";", comment="#", index_col=['cc'])

# plot data
fig, axes = plt.subplots(2, 1, figsize=(6, 6), squeeze=False)

for i, t_leaf in enumerate([20, 35]):
    ax = axes[i, 0]
    ax.plot(meas.index, meas[f'an_{t_leaf:.0f}'], 'o')
    ac_fit = meas[f'ac_{t_leaf:.0f}'].dropna()
    ax.plot(ac_fit.index, ac_fit, '-', label="ac")
    aj_fit = meas[f'aj_{t_leaf:.0f}'].dropna()
    ax.plot(aj_fit.index, aj_fit, '-', label="aj")

    ax.legend(loc='lower right', title=f"t_leaf: {t_leaf:.0f}")
    ax.set_ylim(-5, 35)
    ax.set_xlabel("Ci [µmol.mol-1]")
    ax.set_ylabel("Assimilation [µmol CO2.m-2.s-1]")

fig.tight_layout()
plt.show()
