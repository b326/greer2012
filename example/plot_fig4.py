"""
Figure 4
========

Plot Assimilation light response curves to compare to fig4.
"""
import matplotlib.pyplot as plt
import pandas as pd

from greer2012 import pth_clean

# read data
df = pd.read_csv(pth_clean / "fig4.csv", sep=";", comment="#")

# plot data
fig, axes = plt.subplots(1, 1, figsize=(10, 6), squeeze=False)

ax = axes[0, 0]
for temp, sdf in df.groupby(by='temp'):
    ax.plot(sdf['ppfd'], sdf['assim'], 'o-', label=f"{temp:d}°C")

ax.legend(loc='lower right')
ax.set_ylim(-5, 20)
ax.set_xlabel("PPFD [µmol photon.m-2.s-1]")
ax.set_ylabel("Assimilation [µmol CO2.m-2.s-1]")

fig.tight_layout()
plt.show()
