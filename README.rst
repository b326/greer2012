========================
greer2012
========================

.. {# pkglts, doc

.. image:: https://b326.gitlab.io/greer2012/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/greer2012/0.1.0/

.. image:: https://b326.gitlab.io/greer2012/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/greer2012

.. image:: https://b326.gitlab.io/greer2012/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/greer2012/

.. image:: https://badge.fury.io/py/greer2012.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/greer2012

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/greer2012/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/greer2012/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/greer2012/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/greer2012/commits/main
.. #}

Data and formalisms from Greer et al. (2012)

